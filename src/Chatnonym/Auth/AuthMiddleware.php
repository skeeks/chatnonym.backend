<?php

namespace Chatnonym\Auth;

use \Chatnonym\Core\IOC;
use \Chatnonym\Session\SessionManager;

/**
 *
 * @author sixkn_000
 */
class AuthMiddleware {

    const HTTP_AUTH_HEADER = "Authorization";

    public function __invoke($request, $response, $next) {
        $session = IOC::get(SessionManager::class)->set($session);
        if (empty($session->getUserNr())) {
            $response->withStatus(401); // Unauthorized
        }
        $response = $next($request, $response);
        return $response;
    }

}
