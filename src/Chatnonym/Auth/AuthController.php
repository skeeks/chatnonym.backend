<?php

namespace Chatnonym\Auth;

use \Chatnonym\Core\Config;
use \Chatnonym\Core\IOC;
use \Chatnonym\Session\SessionManager;
use \Chatnonym\Session\Session;

class AuthController {

    protected $ci;

    public function __construct(\Interop\Container\ContainerInterface $ci) {
        $this->ci = $ci;
    }

    public function login($request, $response, $args) {
        return $response;
    }

    public function register($request, $response, $args) {
        // TODO do the register here
        return $response;
    }

    public function logout($request, $response, $args) {
        // TODO do the logout here
        return $response;
    }

}
