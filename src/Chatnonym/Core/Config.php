<?php

namespace Chatnonym\Core;

class Config {

    public static function get($option) {
        global $app;
        return $app->getContainer()->get('settings')->get($option);
    }

}
