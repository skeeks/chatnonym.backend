<?php

namespace Chatnonym\Post;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    // Model Configuration
    protected $table = 'users'; // table name
    public $timestamps = true; // adds created_at and edited_at to the model
    protected $primaryKey = 'user_nr';

}
