<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Chatnonym\Session;

class Session {

    const SESSION_VERSION = 1;

    protected $m_userNr;
    protected $m_username;
    protected $m_sessionVersion;

    public function __construct($userNr, $username, $sessionVersion = self::SESSION_VERSION) {
        $this->m_userNr = $userNr;
        $this->m_username = $username;
        if (!empty($sessionVersion)) {
            $this->m_sessionVersion = $sessionVersion;
        } else {
            $this->m_sessionVersion = SESSION_VERSION;
        }
    }

    public function getUserNr() {
        return $this->m_userNr;
    }

    public function getUsername() {
        return $this->m_username;
    }

    public function getSessionVersion() {
        return $this->m_sessionVersion;
    }

    public function toArray() {
        return array(
            'userNr' => $this->m_userNr,
            'username' => $this->m_username,
            'sessionVersion' => $this->m_sessionVersion
        );
    }

    public static function fromArray($array) {
        return new Session($array->userNr, $array->username, $array->sessionVersion);
    }

}
