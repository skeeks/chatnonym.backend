<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Chatnonym\Session;

class SessionManager {

    private $m_session;
    protected $ci;

    //Constructor
    public function __construct(\Interop\Container\ContainerInterface $ci) {
        $this->ci = $ci;
    }

    public function get() {
        return $this->m_session;
    }

    public function set(Session $session) {
        $this->m_session = $session;
    }

}
