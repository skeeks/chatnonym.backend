<?php

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // Application  Specific Settings
        'app.version' => '0.0.1',
        'jwt.secret_key' => '79FE8DCB81B34551AABFD9A96A4389F8BD2F9E8CE886A4745D64871135',
        'jwt.server_name' => 'localhost',
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'chatnonym',
            'username' => 'chatnonym',
            'password' => 'chatnonym',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ],
    ],
];
