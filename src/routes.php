<?php

// Routes
use \Chatnonym\Core\Config as Config;

// Open routes (no authentification required)
$app->group('/api', function() use ($app) {
    $app->get('/', function($request, $response, $args) {
        $response->withJson(array('description' => 'Chatnonym API', 'version' => Config::get('app.version')));
    });

    $app->get('/login', '\Chatnonym\Auth\AuthController:login');
    $app->post('/register', '\Chatnonym\Auth\AuthController:register');
});

// Protected routes (authentification required)
$app->group('/sapi', function() use ($app) {
    $app->post('/posts/load/{:id}', '\Chatnonym\Post\PostController:load');
    $app->post('/logout', '\Chatnonym\Auth\AuthController:logout');
})->add(new Chatnonym\Auth\AuthMiddleware());
