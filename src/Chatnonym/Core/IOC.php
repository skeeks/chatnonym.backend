<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Chatnonym\Core;

/**
 * Description of IOC
 *
 * @author sixkn_000
 */
class IOC {

    public static function get($key) {
        global $app;
        return $app->getContainer()->get($key);
    }

}
