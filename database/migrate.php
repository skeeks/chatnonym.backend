<?php

use \Illuminate\Database\Capsule\Manager as Capsule;

error_reporting(E_ERROR);
ini_set('display_errors', 'On');

require __DIR__ . '/../vendor/autoload.php';

$_SETTINGS = require __DIR__ . '/../src/settings.php';

$capsule = new Capsule;
echo 'Using Database Connection to \'' . $_SETTINGS['settings']['db']['host'] . '\' with user \'' . $_SETTINGS['settings']['db']['host'] . '\'', "\r\n";

$capsule->addConnection($_SETTINGS['settings']['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();

// Define schema varaible
global $schema;
$schema = $capsule->schema();

echo 'Migrating to the following versions: ', "\r\n";
array_shift($argv);
foreach ($argv as $version) {
    echo "  > Migrating to '$version'... ", "\r\n";
    $filesOrDirs = scandir($version);
    if (!$filesOrDirs) { // if scanndir was not successful, we skip it
        echo '  > Migraton to \'' . $version . '\' FAILED skipped it!', "\r\n";
        continue;
    }
    natsort($filesOrDirs); // Sort alphanumerically the array of files
    $errorCount = 0;
    foreach ($filesOrDirs as $fileOrDir) {
        // check if its a file and if its a .php file
        if (!is_dir($fileOrDir) && (strrpos($fileOrDir, '.php') == (strlen($fileOrDir) - 4))) {
            echo '      Executing php script \'' . $fileOrDir . '\'......';
            try {
                include __DIR__ . '/' . $version . '/' . $fileOrDir;
                echo 'Finished!';
            } catch (Exception $ex) {
                echo 'FAILED! (' . $ex->getMessage() . ')';
                $errorCount++;
            }
            echo "\r\n";
        }
    }
    echo "  > Migration to '$version' FINISHED with $errorCount errors ", "\r\n";
}
echo 'Migration finished!', "\r\n";
