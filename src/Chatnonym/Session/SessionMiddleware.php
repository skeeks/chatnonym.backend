<?php

namespace Chatnonym\Session;

use \Chatnonym\Core\IOC;
use Chatnonym\Core\Config;
use \Firebase\JWT\JWT;

/**
 *
 * @author sixkn_000
 */
class SessionMiddleware {

    const HTTP_AUTH_HEADER = "Authorization";

    public function __invoke($request, $response, $next) {
        $session = $this->getSession($request);
        if (is_null($session)) {
            $session = new Session(null, null);
        }
        IOC::get(SessionManager::class)->set($session);
        $response = $next($request, $response);
        $jwt = $this->sessionToJWT(IOC::get(SessionManager::class)->get());

        $response = $response->withHeader(self::HTTP_AUTH_HEADER, "Bearer " . $jwt);
        return $response;
    }

    public function getSession($request) {
        $authHeader = $request->getHeaderLine(self::HTTP_AUTH_HEADER);
        if (empty(trim($authHeader))) {
            return null;
        }
        $jwtStr = sscanf($authHeader, 'Bearer %s');
        return $this->jwtToSession($jwtStr);
    }

    /**
     * Encodes a session object into a JWT string
     */
    public function sessionToJWT($session) {
        $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt;             //Adding 10 seconds
        $expire = $notBefore + (60 * 60 * 24 * 14);            // Adding 2 weeks
        $serverName = Config::get('jwt.server_name'); // Retrieve the server name from config file

        /*
         * Create the token as an array
         */
        $data = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            'jti' => $tokenId, // Json Token Id: an unique identifier for the token
            'iss' => $serverName, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => is_null($session) ? array() : $session->toArray()  // Data related to the signer user
        ];

        $jwt = JWT::encode(
                        $data, //Data to be encoded in the JWT
                        base64_decode(Config::get('jwt.secret_key')), // The signing key
                        'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );

        return $jwt;
    }

    /**
     * Decodes a jwt string into an object.
     */
    public function jwtToSession($jwt) {
        $secretKey = base64_decode(Config::get('jwt.secret_key'));
        $decodedJwt = JWT::decode($jwt, $secretKey, array('HS512'));

        return Session::fromArray($decodedJwt->data);
    }

}
