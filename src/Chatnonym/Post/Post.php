<?php

namespace Chatnonym\Post;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    // Model Configuration
    protected $table = 'posts'; // table name
    public $timestamps = true; // adds created_at and edited_at to the model
    protected $primaryKey = 'post_nr';

}
