<?php

$schema->dropIfExists('posts');

$schema->create('posts', function($table) {
    $table->bigIncrements('post_nr');
    $table->string('user_nr');
    $table->text('content');
    $table->string('location');
    $table->timestamps();
});
