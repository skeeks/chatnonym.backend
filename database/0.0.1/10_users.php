<?php

/**
 * Migration for users table
 */
$schema->dropIfExists('users');

$schema->create('users', function($table) {
    $table->bigIncrements('user_nr');
    $table->string('username');
    $table->string('password');
    $table->timestamps();
});
